GovCMS Security
---------------
This isn't an official GovCMS project.

This module was developed to improve the security options of a GovCMS site.

User Reports
------------
Please post to the issue queue to help make the module better.

Feel free to provide patches and suggestions too.
