# Stage 1: Build the application
FROM drupal:10-php8.1 as builder

# Set timezone to Australia/Sydney by default
RUN ln -sf /usr/share/zoneinfo/Australia/Sydney /etc/localtime

# Install required packages and PHP extensions
RUN \
    --mount=type=cache,target=/var/cache/apt \
    apt-get update && \
    apt-get install -y --no-install-recommends libicu-dev sqlite3 mariadb-client git unzip rsync sudo && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    docker-php-ext-configure intl && \
    docker-php-ext-install intl

# Configure PHP settings
RUN echo "memory_limit = 512M" >> /usr/local/etc/php/conf.d/docker-php-ram-limit.ini && \
    echo "upload_max_filesize = 100M" >> /usr/local/etc/php/conf.d/docker-php-upload-limit.ini && \
    echo "post_max_size = 100M" >> /usr/local/etc/php/conf.d/docker-php-upload-limit.ini

# Set Composer environment variables
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_MEMORY_LIMIT=-1
ENV SIMPLETEST_BASE_URL="http://localhost"
ENV SIMPLETEST_DB='mysql://drupal:drupal@mariadb/drupal'

# Set the PATH environment variable
ENV PATH=${PATH}:/opt/drupal/vendor/bin

# Set the working directory
WORKDIR /opt/drupal

# Install Composer dependencies
RUN \
    --mount=type=cache,mode=0777,target=/root/.composer/cache \
    composer config allow-plugins.phpstan/extension-installer true && \
    composer config minimum-stability dev && \
    composer require palantirnet/drupal-rector drupal/core-dev:"^10.1" mglaman/drupal-check --dev && \
    cp vendor/palantirnet/drupal-rector/rector.php .

# Stage 2: Final application image
FROM builder as site

# Set the working directory
WORKDIR /opt/drupal

# Copy the rest of the application files
COPY . web/modules/develop/govcms_security

# Adjust ownership
RUN chown -R www-data:www-data web/sites web/modules web/themes